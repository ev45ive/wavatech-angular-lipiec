import { TestBed, inject, async } from "@angular/core/testing";
import { MusicService } from "./music.service";
import { HttpClientModule } from "@angular/common/http";
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";
import { AuthService } from "../auth.service";

describe("MusicService", () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, HttpClientTestingModule],
      providers: [
        {
          provide: "SEARCH_URL",
          useValue: "FAKE_SEARCH_URL"
        },
        {
          provide: AuthService,
          useValue: {
            getToken() {},
            authorize() {}
          }
        },
        MusicService
      ]
    });
  });

  let authSpy: jasmine.Spy;
  beforeEach(inject([AuthService], (auth: AuthService) => {
    authSpy = spyOn(auth, "getToken");
  }));

  it("should create", inject([MusicService], (service: MusicService) => {
    expect(service).toBeDefined();
  }));

  it("should fetch albums from server", async(
    inject(
      [MusicService, HttpTestingController],
      (service: MusicService, ctrl: HttpTestingController) => {
        authSpy.and.returnValue("FAKE_TOKEN");

        const albumsSpy = jasmine.createSpy("Albums Spy");

        service.getAlbums("batman").subscribe(albumsSpy);

        // const request = ctrl.expectOne(req => {
        //   return req.url == "FAKE_SEARCH_URL";
        // });
        const request = ctrl.expectOne("FAKE_SEARCH_URL?type=album&q=batman");

        expect(authSpy).toHaveBeenCalled()

        expect(request.request.headers.get("Authorization")).toEqual(
          "Bearer FAKE_TOKEN"
        );

        request.flush({
          albums: {
            items: ["Placki"]
          }
        },{
          // status:200
        });

        expect(albumsSpy).toHaveBeenCalledWith(["Placki"]);

        ctrl.verify();
      }
    )
  ));
});
