import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import {
  FormControl,
  FormGroup,
  FormArray,
  FormBuilder,
  Validators,
  ValidatorFn,
  AsyncValidatorFn,
  Validator,
  ValidationErrors,
  AbstractControl
} from "@angular/forms";
import { filter, debounceTime, mapTo, withLatestFrom } from "rxjs/operators";
import { Observable, PartialObserver } from "rxjs";

@Component({
  selector: "app-search-form",
  template: `
    <div class="form-group mb-3" [formGroup]="queryForm">
      <input type="text" class="form-control" placeholder="Search" formControlName="query">
      <div class="mt-1 errors" *ngIf="queryForm.get('query').dirty || queryForm.get('query').touched">
        <div *ngIf="queryForm.get('query').hasError('required')">Field is required</div>
        <div *ngIf="queryForm.get('query').getError('minlength') as error">
        Value is too short. Minimum {{ error.requiredLength }} characters required
        </div>
        <div *ngIf="queryForm.get('query').getError('censor') as error">
        Words like "{{error.badword}}" are not allowed</div>
      </div>
      <div *ngIf="queryForm.pending">Please wait...</div>
    </div>
  `,
  styles: [
    `
      .ng-touched.ng-invalid,
      .ng-dirty.ng-invalid {
        border-color: red;
        color: red;
      }
      .errors {
        color: red;
      }
    `
  ]
})
export class SearchFormComponent implements OnInit {
  queryForm: FormGroup;

  constructor() {
    const censor = (badword): ValidatorFn => (
      control: AbstractControl
    ): ValidationErrors => {
      const hasErrors = control.value.includes(badword);

      return hasErrors ? { censor: { badword } } : null;
    };

    const asyncCensor = (badword): AsyncValidatorFn => (
      control: AbstractControl
    ) => {
      // return this.http.get('jakas/validacja',{params}).pipe(map(...))
      const value = control.value;

      return Observable.create(
        (observer: PartialObserver<ValidationErrors | null>) => {
          setTimeout(() => {
            const hasErrors = value.includes(badword);
            const result = hasErrors ? { censor: { badword } } : null;

            observer.next(result);
            observer.complete();
          }, 1000);
        }
      );
    };

    this.queryForm = new FormGroup({
      query: new FormControl(
        "",
        [
          Validators.required,
          Validators.minLength(3)
          // censor("superman")
        ],
        [asyncCensor("batman")]
      )
    });

    // console.log(this.queryForm);

    const value$ = this.queryForm.get("query").valueChanges.pipe(
      filter(query => query.length >= 3)
    );

    const valid$ = this.queryForm.statusChanges.pipe(
      debounceTime(400),
      filter(status => status == "VALID"),
      mapTo(true)
    );

    valid$
      .pipe(withLatestFrom(value$, (valid, value) => value))
      .subscribe(query => this.search(query));

    // .subscribe(status => console.log(status));
  }

  @Output() queryChange = new EventEmitter();

  search(query) {
    this.queryChange.emit(query);
  }

  ngOnInit() {}
}
