import { Component, OnInit, Input } from "@angular/core";
import { Album } from "../model/album";

@Component({
  selector: "app-album-grid",
  template: `
  <div class="card-group">
    <app-album-item class="card"
        *ngFor="let album of albums"
        [album]="album">
    </app-album-item>
  </div>
  `,
  styles: [
    `
      .card {
        flex-basis: 25%;
      }
    `
  ]
})
export class AlbumGridComponent implements OnInit {
  
  @Input()
  albums: Album[] = [];

  constructor() {}

  ngOnInit() {}
}
