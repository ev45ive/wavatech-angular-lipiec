import { TestBed, inject } from "@angular/core/testing";

import { AuthService } from "./auth.service";
import { DOCUMENT } from "@angular/common";

describe("AuthService", () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: DOCUMENT,
          useValue: {
            location: {
              replace() {}
            }
          }
        },
        {
          provide: "Storage",
          useValue: window.localStorage
        },
        {
          provide: "authorization_params",
          useValue: {
            client_id: "123",
            response_type: "token",
            redirect_uri: "http://localhost/"
          }
        },
        {
          provide: "authorization_url",
          useValue: "https://awesome.com/"
        },
        AuthService
      ]
    });
  });

  it("should be created", inject([AuthService], (service: AuthService) => {
    expect(service).toBeTruthy();
  }));

  it("should redirect to auth url with auth params", inject(
    [AuthService, DOCUMENT],
    (service: AuthService, document: Document) => {
      expect(service).toBeTruthy();

      const spy = spyOn(document.location, "replace");

      service.authorize();

      expect(spy).toHaveBeenCalledWith(
        "https://awesome.com/?client_id=123&response_type=token&redirect_uri=http://localhost/"
      );
    }
  ));
});
