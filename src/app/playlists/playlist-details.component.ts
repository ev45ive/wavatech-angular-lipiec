import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Playlist } from "../model/playlist";

@Component({
  selector: "app-playlist-details",
  template: `
  <ng-container [ngSwitch]="mode">
    <div *ngSwitchDefault>
      <h3>Playlist</h3>  

      <dl>
        <dt>Name</dt>
        <dd>{{playlist.name}}</dd>
      </dl>
      <dl>
        <dt>Favourite</dt>
        <dd>{{ playlist.favourite? "Yes" : "No" }}</dd>
      </dl>
      <dl>
        <dt>Color</dt>
        <dd
          [ngStyle]="{
            color: playlist.color,
            borderBottom: '1px solid black' 
          }">
          {{playlist.color}}
        </dd>
      </dl>
      <input type="button" class="btn btn-info" value="Edit" (click)="edit()">
    </div>
    
    <form *ngSwitchCase=" 'edit' " #formRef="ngForm">
      <h3>Edit Playlist</h3>

      <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control" [ngModel]="playlist.name" name="name">
      </div>
      <div class="form-group">
        <label>Favourite </label>
        <input type="checkbox" [ngModel]="playlist.favourite" name="favourite">
      </div>
      <div class="form-group">
        <label>Color </label>
        <input type="color" [ngModel]="playlist.color" name="color">
      </div>
      
      <input type="button" class="btn btn-danger" value="Cancel" (click)="cancel()">
      <input type="button" class="btn btn-success" value="Save" (click)="save(formRef)">
    </form>
    </ng-container>
  `,
  styles: []
})
export class PlaylistDetailsComponent implements OnInit {

  @Input()
  playlist: Playlist;

  constructor() {}

  ngOnInit() {}

  mode = 'show'

  edit(){
    this.mode = 'edit'
  }

  cancel(){
    this.mode = 'show'
  }

  @Output()
  onSave = new EventEmitter<Playlist>()

  save(form){
    const playlist = {
      ...this.playlist,
      ...form.value
    }
    this.onSave.emit(playlist)
    this.mode = 'show'
  }

}
