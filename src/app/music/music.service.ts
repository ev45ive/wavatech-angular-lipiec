import { Injectable, Inject } from "@angular/core";
import { Album } from "../model/album";
import { HttpClient } from "@angular/common/http";
import { AuthService } from "../auth.service";
import { Observable, of, throwError } from "rxjs";
import { map, catchError } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class MusicService {
  albums: Album[] = [
    {
      id: "123",
      name: "Album from service!",
      artists: [],
      images: [
        {
          url: "http://via.placeholder.com/300x300/f00",
          width: 300,
          height: 300
        }
      ]
    },
    {
      id: "234",
      name: "Placki 2",
      artists: [],
      images: [
        {
          url: "http://via.placeholder.com/300x300/0f0",
          width: 300,
          height: 300
        }
      ]
    }
  ];

  // url:string

  constructor(
    @Inject("SEARCH_URL") private url: string,
    private http: HttpClient,
    private auth: AuthService
  ) {
    // this.url = url
  }

  getAlbums(query = "batman"): Observable<Album[]> {
    const token = this.auth.getToken();
    if (!token) {
      return;
    }

    return this.http
      .get<AlbumsResponse>(this.url, {
        headers: {
          Authorization: "Bearer " + token
        },
        params: {
          type: "album",
          q: query
        }
      })
      .pipe(
        map(response => response.albums.items),
        catchError((err, caught) => {
          this.auth.authorize()
          // return of([])
          return throwError(err)
        })
      );
  }
}
// import { map } from "rxjs/operators"

interface AlbumsResponse {
  albums: { items: Album[] };
}
