import { DOCUMENT } from "@angular/common";
import { Injectable, Inject } from "@angular/core";
import { HttpParams } from "@angular/common/http";
// https://developer.spotify.com/documentation/general/guides/authorization-guide/#implicit-grant-flow

@Injectable({
  providedIn: "root"
})
export class AuthService {
  constructor(
    @Inject(DOCUMENT) private document: Document,
    @Inject("Storage") private storage: Storage,
    @Inject("authorization_params") private authorization_params,
    @Inject("authorization_url") private url
  ) {}


  authorize() {
    const p = new HttpParams({
      fromObject: this.authorization_params
    });

    this.storage.removeItem("token");

    this.document.location.replace(this.url + "?" + p.toString());
  }

  token = null;

  getToken() {
    this.token = JSON.parse(this.storage.getItem("token"));
    if (this.token) {
      return this.token;
    }

    const hash = this.document.location.hash.substr(1);
    window.location.hash = "";

    if (hash) {
      const p = new HttpParams({
        fromString: hash
      });
      this.token = p.get("access_token");

      if (this.token) {
        this.storage.setItem("token", JSON.stringify(this.token));
        return this.token;
      }
    }

    this.authorize();
  }
}
