import { Component, OnInit } from "@angular/core";
import { Playlist } from "../model/playlist";
import { ActivatedRoute, Router } from "@angular/router";
import { map } from "rxjs/operators";

// Visual studio code preferences:
//
// "emmet.includeLanguages": {
//   "typescript":"html"
// },

@Component({
  selector: "app-playlists",
  template: `
    <div class="row">
      <div class="col">

        <app-item-list 
          [items]="playlists"
          [selected]="selected"
          (selectedChange)="select($event)">
        </app-item-list>
      
      </div>
      <div class="col">

        <p *ngIf="!selected" class="alert alert-info">
          Please select playlist!
        </p>
        <app-playlist-details 
            *ngIf="selected"
            (onSave)=" save( $event )"
            [playlist]="selected">
        </app-playlist-details>

      </div>
    </div>
  `,
  styles: []
})
export class PlaylistsComponent implements OnInit {
  save(playlist) {
    const index = this.playlists.findIndex(p => p.id == playlist.id);
    this.playlists.splice(index, 1, playlist);
    this.selected = playlist;
  }

  selected = null;

  playlists: Playlist[] = [
    {
      id: 123,
      name: "Angular greatest Hits!",
      favourite: false,
      color: "#ff0000"
    },
    {
      id: 234,
      name: "The best of Angular!",
      favourite: true,
      color: "#00ff00"
    },
    {
      id: 345,
      name: "Angular TOP 20!",
      favourite: false,
      color: "#0000ff"
    }
  ];

  select(playlist) {
    // this.selected = playlist;
    this.router.navigate(["/playlists", playlist.id]);
  }

  // <a [routerLink]="['/playlists',selected.id]" ></a>

  constructor(
    private route: ActivatedRoute, 
    private router: Router
  ) {

    // const id = parseInt(this.route.snapshot.paramMap.get("id"));

    this.route.paramMap.subscribe(paramMap=>{
      const id = parseInt(paramMap.get('id'))
      
      this.selected = this.playlists.find(
        p => p.id == id
      )
    })

    /* var selectedChange = this.route.paramMap.pipe(
      map( paramMap => parseInt(paramMap.get('id'))),
      map( id => this.playlists.find(
        p => p.id == id
      ))
    )

    selectedChange.subscribe(playlist => this.selected = playlist) */

  }

  ngOnInit() {}
}
