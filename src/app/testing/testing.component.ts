import { Observable } from "rxjs";
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  Inject
} from "@angular/core";

@Component({
  selector: "app-testing",
  template: `
    <p class="message">{{message}}</p>
    
    <input type="text" class="input" 
    [(ngModel)]="message" name="my-input">

    <example-component [message]="message">
    </example-component>

    <button class="reset" (click)="reset()">
      Reset
    </button>
  `,
  styles: []
})
export class TestingComponent implements OnInit {
  @Input() message = "testing works!";

  @Output() notification = new EventEmitter();

  notify() {
    this.notification.emit();
  }

  reset() {
    this.message = "";
  }

  constructor(
    @Inject("MessagesService")
    private service: {
      getMessages(): Observable<string>;
    }
  ) {}

  ngOnInit() {
    this.service.getMessages().subscribe(msg => {
      this.message = msg;
    });
  }
}
