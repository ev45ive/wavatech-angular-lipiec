export interface Playlist {
  id: number;
  name: string;
  favourite: boolean;
  color: string;
  /**
   * List of tracks 
   */
  tracks?: Track[]
  // tracks: Track[] | null
}

export interface Track{
  id: number;
  name: string;
}
