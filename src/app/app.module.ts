import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { PlaylistsModule } from "./playlists/playlists.module";
import { MusicModule } from "./music/music.module";
import { AppRoutingModule } from "./app-routing.module";
import { AuthService } from "./auth.service";
import { TestingComponent } from "./testing/testing.component";

@NgModule({
  declarations: [
    AppComponent
    // TestingComponent
  ],
  imports: [BrowserModule, AppRoutingModule, PlaylistsModule, MusicModule],
  providers: [
    {
      provide: "Storage",
      useValue: window.localStorage
    },
    {
      provide: "authorization_params",
      useValue: {
        client_id: "b2266c1dac374f7f99e919fde540ce96",
        response_type: "token",
        redirect_uri: "http://localhost:4200/"
      }
    },
    {
      provide: "authorization_url",
      useValue: "https://accounts.spotify.com/authorize"
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private auth: AuthService) {
    this.auth.getToken();
  }
}
