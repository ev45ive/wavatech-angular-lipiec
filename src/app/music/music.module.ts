import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicSearchComponent } from './music-search.component';
import { SearchFormComponent } from './search-form.component';
import { AlbumGridComponent } from './album-grid.component';
import { AlbumItemComponent } from './album-item.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  declarations: [
    MusicSearchComponent, 
    SearchFormComponent, 
    AlbumGridComponent, 
    AlbumItemComponent
  ],
  exports: [MusicSearchComponent],
  providers:[
    {
      provide: 'SEARCH_URL',
      useValue:'https://api.spotify.com/v1/search'
    },
    /* {
      provide: 'MusicService',
      useFactory: (url)=>{
        return new MusicService(url)
      },
      deps:['SEARCH_URL']
    }, */
    /* {
      provide:'MusicService',
      useClass: MusicService
    }, */
    /* {
      provide: MusicService,
      useClass: MusicService
    }, */
    // MusicService
  ]
})
export class MusicModule { }
