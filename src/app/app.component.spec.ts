import { TestBed, async, ComponentFixture } from "@angular/core/testing";
import { AppComponent } from "./app.component";
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from "@angular/core";
import { RouterModule } from "@angular/router";
import { APP_BASE_HREF } from "@angular/common";
describe("AppComponent", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [RouterModule.forRoot([], {})],
      providers: [{ provide: APP_BASE_HREF, useValue: "/" }],
      schemas: [
        // Shallow Component Testing:
        CUSTOM_ELEMENTS_SCHEMA
      ]
    }).compileComponents();
  }));
  let fixture: ComponentFixture<AppComponent>;
  let app: AppComponent;
  let compiled: HTMLElement;

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.debugElement.componentInstance;
    compiled = fixture.debugElement.nativeElement;
  });

  it("should create the app", async(() => {
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'app'`, async(() => {
    expect(app.title).toEqual("Music App");
  }));

  it("should render title in a navbar", async(() => {
    fixture.detectChanges();
    expect(compiled.querySelector(".navbar-brand").textContent).toContain(
      "Music App"
    );
  }));
});
