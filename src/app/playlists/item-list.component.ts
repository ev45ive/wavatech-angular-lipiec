import { Component, OnInit, ViewEncapsulation, Input, EventEmitter, Output } from "@angular/core";
import { Playlist } from "../model/playlist";

@Component({
  selector: "app-item-list",
  template: `
    <div class="list-group">
      <div class="list-group-item" *ngFor="let playlist of playlists index as i" 
        [class.active]=" playlist == selected "
        (click)=" select(playlist) ">

        {{i+1}}. {{playlist.name}}
      </div>
    </div>
    <!-- {{ selected?.name}} -->
  `,
  encapsulation: ViewEncapsulation.Emulated,
  styles: [],
  // inputs:[
  //   'playlists:items'
  // ]
})
export class ItemListComponent implements OnInit {

  @Input('items') 
  playlists: Playlist[] = [];
  
  @Input()
  selected = null

  @Output()
  selectedChange = new EventEmitter<Playlist>()

  select(playlist){
    // this.selected = playlist
    this.selectedChange.emit(playlist)
  }
  
  constructor() {}

  ngOnInit() {}
}
