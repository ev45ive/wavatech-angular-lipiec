import {
  async,
  ComponentFixture,
  TestBed,
  inject
} from "@angular/core/testing";

import { TestingComponent } from "./testing.component";
import { DebugElement, Component, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { By } from "@angular/platform-browser";
import { FormsModule, NgModel } from "@angular/forms";
import { Observable, of, empty, Subject } from "rxjs";

/* @Component({
  selector:'example-component',
  template:'',
  inputs:['message:message']
})
class ExampleComponentMock{} */

describe("TestingComponent", () => {
  let debugElement: DebugElement;
  let component: TestingComponent;
  let fixture: ComponentFixture<TestingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestingComponent],
      schemas: [
        // Shallow Component Testing:
        CUSTOM_ELEMENTS_SCHEMA
      ],
      imports: [FormsModule],
      providers: [
        {
          provide: "MessagesService",
          useValue: {
            getMessages() {
              return new Subject()
            }
          }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestingComponent);
    debugElement = fixture.debugElement;
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should render some text", () => {
    const pElem = debugElement.query(By.css(".message"));
    expect(pElem.nativeElement.innerText).toBe("testing works!");
  });

  it("should render text in binding", () => {
    component.message = "Ala ma placki!";

    fixture.detectChanges();

    const pElem = debugElement.query(By.css(".message"));
    expect(pElem.nativeElement.innerText).toBe("Ala ma placki!");
  });

  it("should update message from user input", () => {
    const iElem = debugElement.query(By.css(".input"));
    const pElem = debugElement.query(By.css(".message"));

    iElem.nativeElement.value = "Zmieniam input!";

    fixture.autoDetectChanges();

    // Native event (no control over properities)
    iElem.nativeElement.dispatchEvent(new Event("input"));

    expect(component.message).toEqual("Zmieniam input!");

    // Wait for asynchronous ngModel
    return fixture.whenStable().then(() => {
      expect(pElem.nativeElement.innerText).toBe("Zmieniam input!");
      //done()
    });
  });

  it("should sync message to input value", () => {
    // const iElem = debugElement.query(By.css(".input"));
    // const iElems = debugElement.queryAll(By.directive(NgModel));
    const iElem = debugElement.query(By.directive(NgModel));
    const model = iElem.injector.get(NgModel);
    model.name;

    component.message = "Ala ma placki!";
    fixture.detectChanges();

    // Wait for asynchronous ngModel
    return fixture.whenStable().then(() => {
      expect(iElem.nativeElement.value).toEqual("Ala ma placki!");
    });
  });

  it("should reset message on button click", () => {
    const resetBtn = debugElement.query(By.css(".reset"));

    //const spy = spyOn(component,'reset').and.returnValue('return test')
    // spyOnProperty(component,'reset')
    const spy = spyOn(component, "reset").and.callThrough();

    // resetBtn.nativeElement.dispatchEvent(new MouseEvent("click"));
    resetBtn.triggerEventHandler("click", {});

    expect(component.reset).toHaveBeenCalled();

    expect(component.message).toBe("");
  });

  it("should emit notifications", () => {
    const spy = jasmine.createSpy("notification");

    component.notification.subscribe(spy);
    component.notify();

    expect(spy).toHaveBeenCalled();
  });

  interface MessagesService {
    getMessages(): Observable<string>;
  }

  it("should load message from MessagesService", inject(
    ["MessagesService"],
    service => {
      // const s = fixture.componentRef.injector.get('MessagesService')

      const messages = new Subject()

      const spy = spyOn(service,'getMessages').and.returnValue(messages)

      component.ngOnInit()
      expect(spy).toHaveBeenCalled()
      expect(messages.observers.length).toEqual(1)

      messages.next("Message from service")
      expect(component.message).toBe("Message from service");
    }
  ));
});
