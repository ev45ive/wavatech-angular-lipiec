import { Component, OnInit, Inject } from "@angular/core";
import { MusicService } from "./music.service";
import { Album } from "../model/album";

@Component({
  selector: "app-music-search",
  template: `
  <div class="row">
  <div class="col">
    <app-search-form (queryChange)="search($event)">
    </app-search-form>

  </div>
</div>
<div class="row">
  <div class="col">
    <div class="alert alert-danger" *ngIf="error">
    {{ error}}
    </div>
     <app-album-grid [albums]="albums">
     </app-album-grid>
  </div>
</div>
  `,
  styles: []
})
export class MusicSearchComponent implements OnInit {
  albums: Album[];

  constructor(private musicService: MusicService) {}

  error;

  search(query) {
    this.musicService
      .getAlbums(query)
      .subscribe(
        albums => (this.albums = albums),
        resp => (this.error = resp.error.error.message)
      );
  }

  ngOnInit() {}
}
