// https://developer.spotify.com/documentation/web-api/reference/object-model/#album-object-simplified

export interface Album{
  id: string;
  name: string;
  artists: Artist[]
  images: AlbumImage[]
}

export interface Artist{
  id: string;
  name: string;
}

export interface AlbumImage{
   url: string;
   width: number;
   height: number;
}